#!/bin/bash
################################################################################
# Copyright 2022 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
# runs the install-build-deps.sh script if it's present based on arguments from
# the CI job and tag name if applicable. This allows CI to build packages with
# different SDK release targets and different dependencies.
#
# designed to be run from the gitlab runner by CI
# takes two arguments:
#   platform (qrb5165, qrb5165-2, or apq8096 for now)
#   repo section or tag name
#      - this can be something like dev, staging, or a tag name like sdk-1.0
################################################################################

set +e

WORK_DIR=${PWD}
echo "[INFO] - check-build-deps-deb.sh --> running from: " $WORK_DIR

# nothing to do if there is no builds deps script
if ! [[ -e install_build_deps.sh ]];then
	echo "[INFO] No install_build_deps.sh script found. Moving on. "
	exit 0
fi

PLATFORM=$1
# check platform is currently supported and warn if otherwise
if [ ! "$PLATFORM" == "qrb5165" ] && [ ! "$PLATFORM" == "qrb5165-2" ] && [ ! "$PLATFORM" ==  "apq8096" ]; then
	echo "WARNING, unrecognized PLATFORM: $PLATFORM"
	echo "should be one of qrb5165, qrb5165-2, or apq8096"
	echo "continuing anyway, but with undefined behavior"
fi


# decide which repo to pull dependencies from based on git tag
SECTION=$2
if [[ "$SECTION" == "dev" ]] || [[ "$SECTION" == "stable" ]] || [[ "$SECTION" == "staging" ]]; then
	echo "[INFO] detected explicit repo: $SECTION"

elif [[ "$SECTION" =~ ^sdk-[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3} ]]; then
	echo "detected 3-digit sdk repo tag for an sdk patch: $SECTION"
	SECTION=${SECTION%.*} ## trim off last patch number
	echo "pulling dependencies from $SECTION repo"

elif [[ "$SECTION" =~ ^v[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$ ]];then
	echo "[INFO] detected a version tag ($SECTION) for staging branch"
	SECTION="staging"

else
	echo "[INFO] - received invalid second arg: $2"
	exit 1
fi

echo "[INFO] - check-build-deps-deb.sh using repo : $SECTION"

## busybox has different arguments to the timeout command, so figure out
## which to use. Also timeout prints to stderr since it doesn't have a help
## menu so redirect stderr to stdout for grep to pass.
if timeout --help 2>&1 | /bin/grep -q "BusyBox"; then
	echo "[INFO] - Detected busybox"
	TIMEOUT_ARGS="-t 180"
else
	echo "[INFO] - not busybox"
	TIMEOUT_ARGS="3m"
fi


timeout $TIMEOUT_ARGS ./install_build_deps.sh $PLATFORM $SECTION
if (( $? != 0 )); then
	echo "[WARNING] problem running install_build_deps.sh on the first try"
	echo "waiting for 30 seconds and trying again"
	sleep 30
	timeout $TIMEOUT_ARGS ./install_build_deps.sh $PLATFORM $SECTION
	if (( $? != 0 )); then
		echo "[ERROR] problem running install_build_deps.sh a second time"
		exit 1
	else
		echo "[INFO] install_build_deps.sh worked on the second try"
	fi
fi
echo "[INFO] install_build_deps.sh worked on the first try"
