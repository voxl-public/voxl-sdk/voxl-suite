#!/bin/bash
################################################################################
# Copyright 2022 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
# deploys a deb (or ipk) file to public debian binary repo based on tag and
# branch name. This replaces the old deploy-opkg-repo.sh script
# designed to be run from the gitlab runner by CI
################################################################################


SSH_ARGS="-o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY root@35.215.116.197"



# exit on error
set -e

echo "--> PLATFORM: " $PLATFORM
echo "--> BRANCH:   " $CI_COMMIT_BRANCH
echo "--> TAG:      " $CI_COMMIT_TAG

# check platform is currently supported and warn if otherwise
if [ ! "$PLATFORM" == "qrb5165" ] && [ ! "$PLATFORM" == "qrb5165-2" ] && [ ! "$PLATFORM" ==  "apq8096" ]; then
	echo "WARNING, unrecognized PLATFORM: $PLATFORM"
	echo "should be one of qrb5165, qrb5165-2, or apq8096"
	echo "continuing anyway, but with undefined behavior"
fi


# check a deb was actually build
NUM_PKG=$(ls -1q *.deb *.ipk 2>/dev/null | wc -l)

if [ $NUM_PKG -eq "0" ]; then
	echo "ERROR: missing deb or ipk file"
	exit 1
elif [ $NUM_PKG -gt "1" ]; then
	echo "ERROR: more than 1 deb or ipk file found"
	exit 1
fi


# do deb/ipkg specific checks
FILE_DEB=(*.deb)
FILE_IPK=(*.ipk)
IS_IPK=false

if [ -f "$FILE_DEB" ]; then

	FILE=$FILE_DEB
	if ! [[ $FILE == *_arm64.deb ]]; then
		echo "WARNING, file does not comply with debian package name convention"
		echo "it should end in _arm64.deb"
		echo "fixing this automatically for now"
		NEW=${FILE%.deb}_arm64.deb
		mv $FILE $NEW
		FILE=$NEW
	fi

elif [ -f $FILE_IPK ]; then
	FILE=$FILE_IPK
	IS_IPK=true

else
	echo "ERROR, can't fine either deb or ipk file"
	exit 1
fi
echo "--> FILE:      $FILE"


WORDS=$(echo $CI_COMMIT_TAG | wc -w)

# set destination of the file based on branch and tag
if [[ "$CI_COMMIT_BRANCH" == "dev" ]]; then

	SECTION="dev"

	# for ipks in dev branch, make sure there is a timestamp
	if [ -f $FILE_IPK ]; then
		if ! [[ $FILE =~ _[0-9]{12}.ipk$ ]]; then
			echo "--> INFO: found ipk bound for dev branch without timestamp"
			echo "-->       adding timestamp automatically"
			dts=$(date +"%Y%m%d%H%M")
			NEW=${FILE%.ipk}_$dts.ipk
			mv $FILE $NEW
			FILE=$NEW
			echo "new ipk name with timestamp: $newname"
		fi
	fi

# for nightly builds (voxl-suite only thing)
elif [ $WORDS -eq 0 ] && [ "$CI_COMMIT_BRANCH" == "master" ]; then
	SECTION="staging"

# for branches other than dev (master and maintenance branches) figure out
# what to do based on tag. version tags go to staging
# sdk tags go to sdk repos.
else
	if [ $WORDS -eq 0 ]; then
		echo "ERROR empty commit tag"
		exit 1
	fi

	# grab first word
	TAG_START=$(echo $CI_COMMIT_TAG | awk '{print $1}')

	# versioned tags go to staging
	if [[ $TAG_START =~ ^[vV][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}(.*) ]]; then
		SECTION="staging"
	# sdk tags go to an sdk folder
	elif [[ $TAG_START =~ ^sdk-[0-9]{1,3}.[0-9]{1,3}$ ]]; then
		SECTION=$TAG_START
	elif [[ $TAG_START =~ ^sdk-[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3} ]]; then
		SECTION=${TAG_START%.*} ## trim off last patch number
	else
		echo "WARNING tag should specify version vx.y.z sdk-x.y or sdk-x.y.z"
		echo "skipping deploying this package for unknown tag format"
		exit 0
	fi
fi


## construct the final path on google VM to push to
DEST="/debian/dists/$PLATFORM/$SECTION/binary-arm64/"
echo "--> DEST:      $DEST"

## skip pushing voxl-cross-template, but let it go through all the checks up to this point
if [[ $FILE = voxl-cross-template* ]]; then
	echo "--> INFO skipping deploying voxl-cross-template to repo"
	exit 0;
fi


if ! [ -f $GCLOUD_REPO_VM_SSH_KEY ]; then
	echo "ERROR failed to find ssh key"
	echo "Make sure tag wildcards 'v*' & 'sdk*' are protected"
	echo "as well as dev and master branches"
	exit 1
fi

echo "setting permissions for key"
chmod 400 $GCLOUD_REPO_VM_SSH_KEY


echo "validating ssh is working"
if ssh $SSH_ARGS "test -e /debian/"; then
	echo "SSH worked"
else
	echo "ssh failed"
	exit 1
fi


# checking for lockfile
ISLOCKED=true
ATTEMPTS="10"
SLEEP="20"
for ((i=1; i <= $ATTEMPTS; i++)); do
	if ssh $SSH_ARGS "test -e /debian/lockfile"; then
		echo "deploy attempt $i of $ATTEMPTS: deb repo is locked. sleeping for $SLEEP seconds."
		sleep $SLEEP
	else
		echo "no lockfile found, deploying package"
		ISLOCKED=false
		break
	fi
done

if [ $ISLOCKED == true ]; then
	echo "deb repo still locked after $ATTEMPTS attempts, giving up"
	exit 1
fi

if [[ ! "$CI_COMMIT_TAG" == "sdk"* ]]; then
	# delete other voxl-suite packages
	echo "cleaning old voxl-suite packages"
	ssh -o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY root@35.215.116.197 "cd $DEST && rm -f voxl-suite*"
fi

# push to the repo
echo "pushing using scp"
scp -o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY "$FILE" root@35.215.116.197:$DEST

# for ipk repos, used to set a newfile flag. Not anymore,
# now the reindex step looks for timestamps
# if [ $IS_IPK == true ]; then
# 	touch newfile.flag
# 	echo "pushing newfile.flag with scp"
# 	scp -o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY newfile.flag root@35.215.116.197:$DEST
# fi

echo "DONE"
exit 0
