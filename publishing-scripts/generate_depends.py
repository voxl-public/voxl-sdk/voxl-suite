#!/usr/bin/python3
import argparse
import wget
import sys
import os
import gzip

PACKAGE_BLACKLIST_QRB5165=["voxl-ceres-solver", "voxl-eigen3", "voxl-jpeg-turbo", "voxl-mapper", "voxl-mongoose", "voxl-nlopt", "voxl-suite", "voxl-voxblox", "libvoxl-cci-direct", "mavlink-camera-manager", "voxl-hires-server", "voxl-bind", "voxl-slpi-uart-bridge", "libuvc", "voxl-flir-server", "voxl-googletest", "voxl-ros2-foxy", "voxl-mpa-to-ros2", "voxl-microdds-agent", "voxl-libseek-thermal", "voxl-wifi-broadcast", "voxl-fpv-px4", "voxl-neopixel-manager", "voxl-mainline-px4"]
PACKAGE_BLACKLIST_QRB5165_2=["voxl-ceres-solver", "voxl-eigen3", "voxl-mapper", "voxl-mongoose", "voxl-nlopt", "voxl-suite", "voxl-voxblox", "libvoxl-cci-direct", "mavlink-camera-manager", "voxl-hires-server", "voxl-bind", "voxl-slpi-uart-bridge", "libuvc", "voxl-flir-server", "voxl-googletest", "voxl-ros2-foxy", "voxl-mpa-to-ros2", "voxl-microdds-agent", "voxl-libseek-thermal", "voxl-wifi-broadcast", "voxl-fpv-px4", "voxl-neopixel-manager", "voxl-mainline-px4", "voxl-opencv", "libmodal-cv"]
PACKAGE_BLACKLIST_APQ8096=["apq8096-libpng", "libvoxl-cci-direct", "voxl-boost", "voxl-ceres-solver", "voxl-eigen3", "voxl-jpeg-turbo", "voxl-mapper", "voxl-mongoose", "voxl-nlopt", "voxl-open-vins", "voxl-open-vins-server", "libuvc", "voxl-feature-tracker", "voxl-flow-server", "voxl-mavlink-server", "voxl-hires-server", "voxl-suite", "voxl-voxblox", "libqrb5165-io", "mavlink-camera-manager", "Python_3.6.9"]

# Downloads Packages file, given dist and branch
def download_packages_file(dist, branch):
    # cleanup any existing Packages file
    if os.path.exists("Packages"):
        os.remove("Packages")
    
    if os.path.exists("Packages.gz"):
        os.remove("Packages.gz")
    
    base_url="http://voxl-packages.modalai.com/dists/"
    packages_file_url=""
    if dist == "qrb5165" or dist == "qrb5165-2":
        packages_file_url = base_url + dist + "/" + branch + "/binary-arm64/Packages"
    elif dist == "apq8096":
        packages_file_url = base_url + dist + "/" + branch + "/binary-arm64/Packages.gz"
    else:
        print("[ERROR] Unknown dist:", dist)
        sys.exit(1)

    print("Attempting to download:", packages_file_url)
    try:
        wget.download(packages_file_url)
        print()
    except Exception as e:
        print("\n[ERROR] Failed to download Packages file, ensure dist and branch are valid.")
        print(e)
        sys.exit(1)

# Parses Packages file and returns an array of package info
def parse_file(file_path, dist):
    package_names_arr = []
    final_package_arr = []

    # cleanup any existing Packages file
    if not os.path.exists(file_path):
        print("\n[ERROR] file doesn't exist:", file_path)
        sys.exit(1)

    if dist == "apq8096":
        with gzip.open(file_path, 'rb') as f_in:
            with open("Packages", 'wb') as f_out:
                f_out.write(f_in.read())

        file_path="Packages"

    with open(file_path, 'r') as file:
        file_contents = file.read()
    
    split_array = file_contents.split('\n\n')
    
    PACKAGE_BLACKLIST = ''
    if dist == "qrb5165":
        PACKAGE_BLACKLIST = PACKAGE_BLACKLIST_QRB5165
    elif dist == "qrb5165-2":
        PACKAGE_BLACKLIST = PACKAGE_BLACKLIST_QRB5165_2
    elif dist == "apq8096":
        PACKAGE_BLACKLIST = PACKAGE_BLACKLIST_APQ8096


    # traverse file in reverse, for ease
    for package in reversed(split_array):
        package = package.split("\n")
        
        # remove any empty entries
        if "" in package:
            package.remove("")

        # avoid leading whitespace
        if len(package) > 1:
            package_name = package[0].split(" ")[1]       
            
            # make sure we don't add the same package twice
            if package_name not in package_names_arr:
                if dist == "qrb5165" or dist == "qrb5165-2":
                    # check that package is not blacklisted
                    if package_name not in PACKAGE_BLACKLIST:
                        package_names_arr.append(package_name)
                        package_version = package[2].split(" ")[1]

                        # strip off datetimestamp on dev
                        if branch == "dev":
                            package_version = package_version[:package_version.rfind("-")]
                        final_package_arr.append(package_name + "(>=" + package_version + ")")
                        
                if dist == "apq8096":
                    # check that package is not blacklisted
                    if package_name not in PACKAGE_BLACKLIST:
                        package_names_arr.append(package_name)
                        package_version = package[1].split(" ")[1]
                        final_package_arr.append(package_name + "(>=" + package_version + ")")

    # un-reverse list
    return final_package_arr[::-1]

# takes in new depends string and modifies control file
def modify_control_file(depends_str):
    print("\n\nFinal depends list:\n\n")
    print(depends_str)
   
    control_file_path = dist + "/control/control"

    with open(control_file_path, 'r') as file:
        lines = file.readlines()

    with open(control_file_path, 'w') as file:
        for line in lines:
            if "Depends:" in line:
                file.write("Depends: " + depends_str + '\n')
            else:
                file.write(line)
    

if __name__ == "__main__":  
    parser = argparse.ArgumentParser()
    parser.add_argument("dist", help="qrb5165, qrb5165-2, apq8096")
    parser.add_argument("branch", help="staging, dev")
    args = parser.parse_args()
    
    dist = args.dist
    branch = args.branch

    print("Distribution selected: ", dist)
    print("Branch selected: ", branch)

    print("\nDownloading Packages file...")
    download_packages_file(dist, branch)

    if dist == "qrb5165" or dist == "qrb5165-2":
        package_info_arr = parse_file("Packages", dist)
    elif dist == "apq8096":
        package_info_arr = parse_file("Packages.gz", dist)

    depends_str = ""
    
    for i in range(0, len(package_info_arr) -1):
        depends_str+=(package_info_arr[i] + ", ")

    depends_str+=package_info_arr[len(package_info_arr) -1]

    modify_control_file(depends_str)
