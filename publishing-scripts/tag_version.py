#!/usr/bin/python3
import argparse
import wget
import sys
import os
import gzip


# takes in new depends string and modifies control file
def modify_control_file(dist, tag):
    print("\n\nTag:\n\n")

    # ~ forces beta releases to be recognized as older
    if "beta" in tag:
        tag=tag.replace("-", "~")
        
    print(tag)
   
    control_file_path = dist + "/control/control"

    with open(control_file_path, 'r') as file:
        lines = file.readlines()

    with open(control_file_path, 'w') as file:
        for line in lines:
            if "Version:" in line:
                file.write("Version: " + tag + '\n')
            else:
                file.write(line)
    

if __name__ == "__main__":  
    parser = argparse.ArgumentParser()
    parser.add_argument("dist")
    parser.add_argument("tag")
    args = parser.parse_args()
    
    dist = args.dist
    tag = args.tag

    print("Tag: ", tag)

    modify_control_file(dist, tag)
