# voxl-suite

meta-package with dependencies on all major voxl-support packages.

Information about voxl-suite and its version history available on the [docs page](https://docs.modalai.com/voxl-suite/)

This repository contains tools to download and tar-up the suite packages and make a "platform release" including system image and installer.


## overview

This package structure differs from every other template since it must build a package for multiple hardware platforms with different control files. Since CI passes the hardware platform name to the build script instead of the make package script, we use the build script here to build the package, and make_package.sh is empty.

Also, unlike other repos, voxl-suite keeps dev and master parallel since dev voxl-suite packages use >= dependencies to allow smooth upgrades, while stable voxl-suite packages specify == dependencies to ensure a specific voxl-sdk release contains a known set of packages.



## updating voxl-suite

checkout the dev or master branch depending on if you are making a dev or stable voxl-suite release.

update either or both apq8096 or qrb5165 package control files.

## building

```
./build.sh qrb5165
./build.sh apq8096
```
