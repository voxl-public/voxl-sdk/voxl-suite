#!/bin/bash
################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
#
# Semi-universal script for making a deb and ipk package. This is shared
# between the vast majority of VOXL-SDK packages
#
# Add the 'timestamp' argument to add a date-timestamp suffix to the deb package
# version. This is used by CI for making nightly and development builds.
#
# author: james@modalai.com
################################################################################

set -e # exit on error to prevent bad ipk from being generated


## this list is just for tab-completion
AVAILABLE_PLATFORMS="qrb5165 qrb5165-2 apq8096"

print_usage(){
	echo ""
	echo " Build the current project based on platform target."
	echo ""
	echo " Usage:"
	echo ""
	echo "  ./build.sh apq8096"
	echo "        Build ipk package for apq8096"
	echo ""
	echo "  ./build.sh qrb5165"
	echo "        Build deb package qrb5165"
	echo ""
	echo "  ./build.sh qrb5165-2"
	echo "        Build deb package qrb5165-2"
	echo ""
	echo ""
}



if [ "$#" -ne 1 ]; then
	print_usage
	exit 1
fi

PLATFORM="$1"

# values from GITLAB CI
echo "--> PLATFORM: " $PLATFORM
echo "--> BRANCH:   " $CI_COMMIT_BRANCH
echo "--> TAG:      " $CI_COMMIT_TAG

if [[ "$CI_COMMIT_TAG" == *"sdk-"* ]];
then
	echo -e "\n[INFO] SDK tag received: $CI_COMMIT_TAG"
	# cut to format: sdk-M.m, e.g. sdk-1.0.0 -> sdk-1.0
	REPO=$(echo "$CI_COMMIT_TAG" | cut -f1,2 -d '.')

	# cut off sdk- e.g. sdk-1.0.0 -> 1.0.0
	SDK_VER=$(echo "$CI_COMMIT_TAG" | cut -d '-' -f2-)
	echo "[INFO] SDK Version: $SDK_VER"
else
	if [[ $CI_COMMIT_BRANCH == "master" ]];
	then
		REPO="staging"
	elif [[ $CI_COMMIT_BRANCH == "dev" ]];
	then
		REPO="dev"
	else
		echo "[ERROR] Can't handle branch: $CI_COMMIT_BRANCH"
		exit 1
	fi
fi


case "$PLATFORM" in
	apq8096)
		./clean.sh
		mkdir -p pkg/
		python3 publishing-scripts/generate_depends.py $PLATFORM $REPO
		if [[ "$CI_COMMIT_TAG" == *"sdk-"* ]];
		then
			echo -e "\n[INFO] SDK tag received: $CI_COMMIT_TAG"
			python3 publishing-scripts/tag_version.py apq8096 $SDK_VER
		fi
		cp -R apq8096/* pkg/
		echo -e "\nBelow is the generated control file"
		cat pkg/control/control
		echo "Done building for apq8096"
		;;
	qrb5165)
		./clean.sh
		mkdir -p pkg/
		python3 publishing-scripts/generate_depends.py $PLATFORM $REPO
		if [[ "$CI_COMMIT_TAG" == *"sdk-"* ]];
		then
			echo -e "\n[INFO] SDK tag received: $CI_COMMIT_TAG"
			python3 publishing-scripts/tag_version.py qrb5165 $SDK_VER
		fi
		cp -R qrb5165/* pkg/
		echo "qrb5165" > pkg/platform
		echo -e "\nBelow is the generated control file:\n"
		cat pkg/control/control
		echo "Done building for qrb5165"
		;;
	qrb5165-2)
		./clean.sh
		mkdir -p pkg/
		python3 publishing-scripts/generate_depends.py $PLATFORM $REPO
		if [[ "$CI_COMMIT_TAG" == *"sdk-"* ]];
		then
			echo -e "\n[INFO] SDK tag received: $CI_COMMIT_TAG"
			python3 publishing-scripts/tag_version.py qrb5165-2 $SDK_VER
		fi
		cp -R qrb5165-2/* pkg/
		echo "qrb5165-2" > pkg/platform
		echo -e "\nBelow is the generated control file:\n"
		cat pkg/control/control
		echo "Done building for qrb5165-2"
		;;

	*)
		print_usage
		exit 1
		;;
esac





