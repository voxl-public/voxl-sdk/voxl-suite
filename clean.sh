#!/bin/bash
#
# Modal AI Inc. 2021
# author: james@modalai.com



sudo rm -rf apq8096/control.tar.gz
sudo rm -rf apq8096/data.tar.gz
sudo rm -rf *.ipk
sudo rm -rf *.deb
sudo rm -rf .bash_history
sudo rm -rf pkg
